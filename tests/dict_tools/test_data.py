import logging

import pytest

from dict_tools import data

log = logging.getLogger(__name__)
_b = lambda x: x.encode("utf-8")
_s = lambda x: str(x)
# Some randomized data that will not decode
BYTES = b"1\x814\x10"

# This is an example of a unicode string with й constructed using two separate
# code points. Do not modify it.
EGGS = "\u044f\u0438\u0306\u0446\u0430"

test_data = [
    "unicode_str",
    _b("питон"),
    123,
    456.789,
    True,
    False,
    None,
    EGGS,
    BYTES,
    [123, 456.789, _b("спам"), True, False, None, EGGS, BYTES],
    (987, 654.321, _b("яйца"), EGGS, None, (True, EGGS, BYTES)),
    {
        _b("str_key"): _b("str_val"),
        None: True,
        123: 456.789,
        EGGS: BYTES,
        _b("subdict"): {
            "unicode_key": EGGS,
            _b("tuple"): (123, "hello", _b("world"), True, EGGS, BYTES),
            _b("list"): [456, _b("спам"), False, EGGS, BYTES],
        },
    },
    {_b("foo"): "bar", 123: 456, EGGS: BYTES},
]


def test_circular_refs_dicts():
    test_dict = {"key": "value", "type": "test1"}
    test_dict["self"] = test_dict
    ret = data._remove_circular_refs(ob=test_dict)
    assert ret == {"key": "value", "type": "test1", "self": None}


def test_compare_dicts():
    ret = data.compare_dicts(old={"foo": "bar"}, new={"foo": "bar"})
    assert ret == {}

    ret = data.compare_dicts(old={"foo": "bar"}, new={"foo": "woz"})
    expected_ret = {"foo": {"new": "woz", "old": "bar"}}
    assert ret == expected_ret


def test_dict_equality():
    """
    Test cases where equal dicts are compared.
    """
    test_dict = {"foo": "bar", "bar": {"baz": {"qux": "quux"}}, "frop": 0}
    assert data.recursive_diff(test_dict, test_dict) == {}


def test_mixed_equality():
    """
    Test cases where mixed nested lists and dicts are compared.
    """
    test_data = {
        "foo": "bar",
        "baz": [0, 1, 2],
        "bar": {"baz": [{"qux": "quux"}, {"froop", 0}]},
    }
    assert data.recursive_diff(test_data, test_data) == {}


def test_dict_inequality():
    """
    Test cases where two inequal dicts are compared."""
    dict_one = {"foo": 1, "bar": 2, "baz": 3}
    dict_two = {"foo": 2, 1: "bar", "baz": 3}
    expected_result = {"old": {"foo": 1, "bar": 2}, "new": {"foo": 2, 1: "bar"}}
    assert expected_result == data.recursive_diff(dict_one, dict_two)
    expected_result = {"new": {"foo": 1, "bar": 2}, "old": {"foo": 2, 1: "bar"}}
    assert expected_result == data.recursive_diff(dict_two, dict_one)

    dict_one = {"foo": {"bar": {"baz": 1}}}
    dict_two = {"foo": {"qux": {"baz": 1}}}
    expected_result = {"old": dict_one, "new": dict_two}
    assert expected_result == data.recursive_diff(dict_one, dict_two)
    expected_result = {"new": dict_one, "old": dict_two}
    assert expected_result == data.recursive_diff(dict_two, dict_one)


def test_mixed_inequality():
    """
    Test cases where two mixed dicts/iterables that are different are compared."""
    dict_one = {"foo": [1, 2, 3]}
    dict_two = {"foo": [3, 2, 1]}
    expected_result = {"old": {"foo": [1, 3]}, "new": {"foo": [3, 1]}}
    assert expected_result, data.recursive_diff(dict_one, dict_two)
    expected_result = {"new": {"foo": [1, 3]}, "old": {"foo": [3, 1]}}
    assert expected_result == data.recursive_diff(dict_two, dict_one)

    list_one = [1, 2, {"foo": ["bar", {"foo": 1, "bar": 2}]}]
    list_two = [3, 4, {"foo": ["qux", {"foo": 1, "bar": 2}]}]
    expected_result = {
        "old": [1, 2, {"foo": ["bar"]}],
        "new": [3, 4, {"foo": ["qux"]}],
    }
    assert expected_result == data.recursive_diff(list_one, list_two)
    expected_result = {
        "new": [1, 2, {"foo": ["bar"]}],
        "old": [3, 4, {"foo": ["qux"]}],
    }
    assert expected_result == data.recursive_diff(list_two, list_one)

    mixed_one = {"foo": {0, 1, 2}, "bar": [0, 1, 2]}
    mixed_two = {"foo": {1, 2, 3}, "bar": [1, 2, 3]}
    expected_result = {
        "old": {"foo": {0}, "bar": [0, 1, 2]},
        "new": {"foo": {3}, "bar": [1, 2, 3]},
    }
    assert expected_result == data.recursive_diff(mixed_one, mixed_two)
    expected_result = {
        "new": {"foo": {0}, "bar": [0, 1, 2]},
        "old": {"foo": {3}, "bar": [1, 2, 3]},
    }
    assert expected_result == data.recursive_diff(mixed_two, mixed_one)


def test_dict_ignore():
    """
    Test case comparing two dicts with ignore-list supplied.
    """
    dict_one = {"foo": 1, "bar": 2, "baz": 3}
    dict_two = {"foo": 3, "bar": 2, "baz": 1}
    expected_result = {"old": {"baz": 3}, "new": {"baz": 1}}
    assert expected_result == data.recursive_diff(
        dict_one, dict_two, ignore_keys=["foo"]
    )


def test_mixed_nested_ignore():
    """
    Test case comparing mixed, nested items with ignore-list supplied.
    """
    dict_one = {"foo": [1], "bar": {"foo": 1, "bar": 2}, "baz": 3}
    dict_two = {"foo": [2], "bar": {"foo": 3, "bar": 2}, "baz": 1}
    expected_result = {"old": {"baz": 3}, "new": {"baz": 1}}
    assert expected_result == data.recursive_diff(
        dict_one, dict_two, ignore_keys=["foo"]
    )


def test_ignore_missing_keys_dict():
    """
    Test case ignoring missing keys on a comparison of dicts."""
    dict_one = {"foo": 1, "bar": 2, "baz": 3}
    dict_two = {"bar": 3}
    expected_result = {"old": {"bar": 2}, "new": {"bar": 3}}
    assert expected_result == data.recursive_diff(
        dict_one,
        dict_two,
        ignore_missing_keys=True,
        ignore_order=True,
    )


def test_ignore_missing_keys_recursive():
    """
    Test case ignoring missing keys on a comparison of nested dicts.
    """
    dict_one = {"foo": {"bar": 2, "baz": 3}}
    dict_two = {"foo": {"baz": 3}}
    expected_result = {}
    assert expected_result == data.recursive_diff(
        dict_one,
        dict_two,
        ignore_missing_keys=True,
        ignore_order=True,
    )
    # Compare from dict-in-dict
    dict_two = {}
    assert expected_result == data.recursive_diff(
        dict_one,
        dict_two,
        ignore_missing_keys=True,
        ignore_order=True,
    )
    # Compare from dict-in-list
    dict_one = {"foo": ["bar", {"baz": 3}]}
    dict_two = {"foo": ["bar", {}]}
    assert expected_result == data.recursive_diff(
        dict_one,
        dict_two,
        ignore_missing_keys=True,
        ignore_order=True,
    )


def test_repack_dictlist(lorem_ipsum):
    list_of_one_element_dicts = [
        {"dict_key_1": "dict_val_1"},
        {"dict_key_2": "dict_val_2"},
        {"dict_key_3": "dict_val_3"},
    ]
    expected_ret = {
        "dict_key_1": "dict_val_1",
        "dict_key_2": "dict_val_2",
        "dict_key_3": "dict_val_3",
    }
    ret = data.repack_dictlist(list_of_one_element_dicts)
    assert ret == expected_ret

    # Try with yaml
    yaml_key_val_pair = "- key1: val1"
    ret = data.repack_dictlist(yaml_key_val_pair)
    assert ret == {"key1": "val1"}

    # Make sure we handle non-yaml junk data
    ret = data.repack_dictlist(lorem_ipsum)
    assert ret == {}


def test_subdict_match():
    test_two_level_dict = {"foo": {"bar": "baz"}}
    test_two_level_comb_dict = {"foo": {"bar": "baz:woz"}}
    test_two_level_dict_and_list = {
        "abc": ["def", "ghi", {"lorem": {"ipsum": [{"dolor": "sit"}]}}],
    }
    test_three_level_dict = {"a": {"b": {"c": "v"}}}

    assert data.subdict_match(test_two_level_dict, "foo:bar:baz") is True
    # In test_two_level_comb_dict, 'foo:bar' corresponds to 'baz:woz', not
    # 'baz'. This match should return False.
    assert data.subdict_match(test_two_level_comb_dict, "foo:bar:baz") is False
    # This tests matching with the delimiter in the value part (in other
    # words, that the path 'foo:bar' corresponds to the string 'baz:woz').
    assert data.subdict_match(test_two_level_comb_dict, "foo:bar:baz:woz") is True
    # This would match if test_two_level_comb_dict['foo']['bar'] was equal
    # to 'baz:woz:wiz', or if there was more deep nesting. But it does not,
    # so this should return False.
    assert data.subdict_match(test_two_level_comb_dict, "foo:bar:baz:woz:wiz") is False
    # This tests for cases when a key path corresponds to a list. The
    # value part 'ghi' should be successfully matched as it is a member of
    # the list corresponding to key path 'abc'. It is somewhat a
    # duplication of a test within test_traverse_dict_and_list, but
    # data.subdict_match() does more than just invoke
    # traverse_list_and_dict() so this particular assertion is a
    # sanity check.
    assert data.subdict_match(test_two_level_dict_and_list, "abc:ghi") is True
    # This tests the use case of a dict embedded in a list, embedded in a
    # list, embedded in a dict. This is a rather absurd case, but it
    # confirms that match recursion works properly.
    assert (
        data.subdict_match(test_two_level_dict_and_list, "abc:lorem:ipsum:dolor:sit")
        is True
    )
    # Test four level dict match for reference
    assert data.subdict_match(test_three_level_dict, "a:b:c:v") is True
    # Test regression in 2015.8 where 'a:c:v' would match 'a:b:c:v'
    assert data.subdict_match(test_three_level_dict, "a:c:v") is False
    # Test wildcard match
    assert data.subdict_match(test_three_level_dict, "a:*:c:v") is True


def test_subdict_match_with_wildcards():
    """
    Tests subdict matching when wildcards are used in the expression
    """
    d = {"a": {"b": {"ç": "d", "é": ["eff", "gee", "8ch"], "ĩ": {"j": "k"}}}}
    assert data.subdict_match(d, "*:*:*:*")
    assert data.subdict_match(d, "a:*:*:*")
    assert data.subdict_match(d, "a:b:*:*")
    assert data.subdict_match(d, "a:b:ç:*")
    assert data.subdict_match(d, "a:b:*:d")
    assert data.subdict_match(d, "a:*:ç:d")
    assert data.subdict_match(d, "*:b:ç:d")
    assert data.subdict_match(d, "*:*:ç:d")
    assert data.subdict_match(d, "*:*:*:d")
    assert data.subdict_match(d, "a:*:*:d")
    assert data.subdict_match(d, "a:b:*:ef*")
    assert data.subdict_match(d, "a:b:*:g*")
    assert data.subdict_match(d, "a:b:*:j:*")
    assert data.subdict_match(d, "a:b:*:j:k")
    assert data.subdict_match(d, "a:b:*:*:k")
    assert data.subdict_match(d, "a:b:*:*:*")


def test_traverse_dict():
    test_two_level_dict = {"foo": {"bar": "baz"}}

    assert {"not_found": "nope"} == data.traverse_dict(
        test_two_level_dict, "foo:bar:baz", {"not_found": "nope"}
    )
    assert "baz" == data.traverse_dict(
        test_two_level_dict, "foo:bar", {"not_found": "not_found"}
    )


def test_traverse_dict_and_list():
    test_two_level_dict = {"foo": {"bar": "baz"}}
    test_two_level_dict_and_list = {
        "foo": ["bar", "baz", {"lorem": {"ipsum": [{"dolor": "sit"}]}}]
    }

    # Check traversing too far: salt.utils.data.traverse_dict_and_list() returns
    # the value corresponding to a given key path, and baz is a value
    # corresponding to the key path foo:bar.
    assert {"not_found": "nope"} == data.traverse_dict_and_list(
        test_two_level_dict, "foo:bar:baz", {"not_found": "nope"}
    )
    # Now check to ensure that foo:bar corresponds to baz
    assert "baz" == data.traverse_dict_and_list(
        test_two_level_dict, "foo:bar", {"not_found": "not_found"}
    )
    # Check traversing too far
    assert {"not_found": "nope"} == data.traverse_dict_and_list(
        test_two_level_dict_and_list, "foo:bar", {"not_found": "nope"}
    )
    # Check index 1 (2nd element) of list corresponding to path 'foo'
    assert "baz" == data.traverse_dict_and_list(
        test_two_level_dict_and_list, "foo:1", {"not_found": "not_found"}
    )
    # Traverse a couple times into dicts embedded in lists
    assert "sit" == data.traverse_dict_and_list(
        test_two_level_dict_and_list,
        "foo:lorem:ipsum:dolor",
        {"not_found": "not_found"},
    )

    # Traverse and match integer key in a nested dict
    # https://github.com/saltstack/salt/issues/56444
    assert "it worked" == data.traverse_dict_and_list(
        {"foo": {1234: "it worked"}},
        "foo:1234",
        "it didn't work",
    )
    # Make sure that we properly return the default value when the initial
    # attempt fails and YAML-loading the target key doesn't change its
    # value.
    assert "default" == data.traverse_dict_and_list(
        {"foo": {"baz": "didn't work"}},
        "foo:bar",
        "default",
    )


class TestImmutableNamespacedMap:
    def test_init(self):
        """
        Verify that an init dict is loaded into the namespace
        """
        init_dict = {"a": 2, "b": 4}
        inm = data.ImmutableDict(init_dict)
        assert inm == init_dict

    def test_init_dict(self):
        """
        Verify that a dict is converted into a mutable mapping namespace from self.update
        """
        init_dict = {"k": {}}
        inm = data.ImmutableDict(init_dict)
        assert inm == init_dict
        assert isinstance(inm["k"], data.ImmutableDict)

    def test_setitem(self):
        inm = data.ImmutableDict({})
        with pytest.raises(TypeError):
            inm["k"] = 1

    def test_delitem(self):
        inm = data.ImmutableDict({})
        with pytest.raises(TypeError):
            del inm["k"]

    def test_getitem(self):
        inm = data.ImmutableDict({"k": "v"})
        assert inm.get("k") == "v"

    def test_getattr(self):
        init_dict = {"k": "v"}
        inm = data.ImmutableDict(init_dict)
        assert inm.k == "v"

    def test_getattr_nested(self):
        d = {"d": "val"}
        c = {"c": d}
        b = {"b": c}
        a = {"a": b}
        inm = data.ImmutableDict(a)

        assert inm.a == b
        assert inm.a.b == c
        assert inm.a.b.c == d
        assert inm.a.b.c.d == "val"

    def test_setattr(self):
        inm = data.ImmutableDict({})
        with pytest.raises(TypeError):
            inm.k = "value"

    def test_overwrite_store(self):
        inm = data.ImmutableDict({})
        with pytest.raises(TypeError):
            inm._store = tuple()

    def test_len(self):
        length = 100
        inm = data.ImmutableDict({f"item_{d}": d for d in range(length)})
        assert len(inm) == length
